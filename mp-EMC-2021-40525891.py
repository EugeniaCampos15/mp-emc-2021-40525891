# EMC: 2021
# Nombre del Alumno: Campos Edith Eugenia
# DNI: 40525891
import os


#MENU DE OPCIONES
def menu():
   os.system('cls')
   print('****************************************************') 
   print('**********MENU DE OPCIONES**********')
   print('1- Agregar un empleado')
   print('2- Mostrar los empleados con id mayor o igual a un valor ingresado por el usuario ')
   print('3-  Cambiar el departamento de un empleado ')
   print('4- Ordenar los empleados por nombre en forma descendente utilizando el método bubble sort')
   print('5-Salir ') 
   op = int(input('Ingrese una opción: '))
   while not(op >= 1 and op <= 5): 
        op = int(input('Ingrese una opción: '))
   return op


def pulsarTecla():
    input('Pulse cualquier tecla para continuar...')


# A continuación un ejemplo que muestra la lista. Puede borrarlo            
#for e in empleados:    
    #print(e)

def ingreseId():
   id = input()
   id= id.replace(" ", "")
   if id== '' :
       id= input('Ingrese nuevamente el Id: ')
       id= id.replace(" ", "")
   return id

def validarId(empleados,id):
    valido = True
    for item in empleados:
        if id == item[0]:
            valido = False
            break
    return valido 

def ingreseNombre():
    nombre= input()
    if nombre== '' :
       nombre= input('Ingrese nuevamente su nombre: ')   
    else:
        print('Nombre Ingresado')    
    return nombre

def ingreseEmail():
    email= input()

def ingreseDepartamento():
    departamento=input()
    while not (departamento in ['1','2', '3',]):
     departamento= input ('Ingrese departamento donde trabaja: 1=Gerencia, 2=Ventas, 3=Compras ')
    if departamento=='1' :
      departamento= 'Gerencia'
    elif departamento == '2':
           departamento = 'Ventas'
    else:
       departamento = 'Compras' 
    return departamento

def ingreseSalario():
    salario= int(input())
    while not(salario >= 0):
        salario = int(input('Ingrese el salario correcto: '))
    return salario
 


def agregarEmpleado(empleados):
 print('***********Registro del empleado***********')
 empleados = [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Compras", 3100]
            ]
 respuesta = 's'   
 while (respuesta=='s') or (respuesta=='S'):
      print('Ingrese el id: ') 
      id = ingreseId()
      if  validarId(empleados,id):
         print('Ingrese Nombre: ')
         nombre = ingreseNombre()
         print('Ingrese email: ')
         email = ingreseEmail()
         print('Ingrese departamento donde trabaja: 1=Gerencia, 2=Ventas, 3=Compras ' )
         departamento = ingreseDepartamento()
         print('Ingrese salario: ')
         salario= ingreseSalario( )
         empleados.append([id,nombre,email,departamento,salario])
         print('Agregado correctamente!')
      else:
         print('Id Existente! ')
      respuesta = input('Desea continuar cargando empleados? s/n: ')       
 return empleados


def mostrarempleados(lista) :
 print('Listado de empleados')
 for item in lista:
   print(item)
 print(lista)      


def buscar(empleados,id):
    pos = -1
    for i, item in enumerate(empleados):
        if(item[0] == id):
            pos = i
            break
    return pos  


def cambiardepartamento(empleados):
   print('********* CAMBIO DE DEPARTAMENTO ********* ')
   id = input('Ingrese Id para cambiar el departamento: ')
   pos = buscar(empleados,id) 
   departamento = input('Ingrese el departamento para cambiar: 1=Gerencia, 2=Ventas, 3=Compras ')
   if departamento==1:
        empleados[pos][4] = 'Gerencia'
        print('Departamento cambiado con exito')
   elif departamento==2:
        empleados[pos][4] = 'Ventas'
        print('Departamento cambiado con exito') 
   elif departamento ==3:
        empleados[pos][4] = 'Compras'
        print('Departamento cambiado con exito')   

def ordenarEmpleados():
  n = len(empleados)
  for i in range(n):
    for j in range(0, n-1):
     if empleados[j] > empleados[j+1]:
      empleados[j], empleados[j+1] = empleados[j+1], empleados[j]
  print(empleados)
  return empleados  


#PRINCIPAL
op = 0
empleados = []
while op != 5:
    op = menu()
    if op == 1: 
        agregarEmpleado(empleados)
        pulsarTecla()
    elif op ==2: 
        mostrarempleados(empleados)
        pulsarTecla()
    elif op == 3: 
         cambiardepartamento(empleados)
         pulsarTecla()
    elif op == 4: 
        ordenarEmpleados(empleados)
        pulsarTecla()
    elif op == 5:
        print('EJECUCION FINALIZADA')
        pulsarTecla()
